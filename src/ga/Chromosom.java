/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author ilhammaziz
 */

public class Chromosom {
   private ArrayList<Node> genes;
   private double fitness;
   private final double randNumber=0.01;
   
   public Chromosom(ArrayList<Node> genes){
       this.genes=new ArrayList<>();
       generateChoromosom(genes);
   }
   
   public Chromosom(){
       this.genes=new ArrayList<>();
   }
   
   public final void generateChoromosom(ArrayList<Node> nodes){
        int[] ints = new Random().ints(1, nodes.size()).distinct().limit(nodes.size()-1).toArray();
        for(int i : ints){
            this.genes.add(nodes.get(i));
        }
        
        setFitness();
    }
   
   public void setFitness(){
       double cost=0;
       double demand=0;
       Node prec=genes.get(0);
       
       for(Node gen : genes){
           if(demand + gen.getDemand()<=100){
               demand+=gen.getDemand();
               cost+=getDistance(prec, gen);
               prec=gen;
           }
           else{
               cost+=getDistance(prec,genes.get(0));
               prec=genes.get(0);
               demand=gen.getDemand();
               cost+=getDistance(prec,genes.get(0));
           }
       }
       
       this.fitness=1/(cost+this.randNumber);
   }
   
   public double getFitness(){
       return this.fitness;
   }
   
   public double getDistance(Node src,Node dest){
        return Math.sqrt(Math.pow(dest.getX() - src.getX(), 2) + Math.pow(dest.getY() - src.getY(), 2));
    }
   
   public void printGenes(){
       for (Node gen : genes) {
           System.out.print(gen.getLabel()+" ");
       }
       System.out.println("");
   }
   
   public void setGenes(ArrayList<Node> genes){
       this.genes=genes;
   }
   
   public ArrayList<Node> getGenes(){
       return this.genes;
   }
}
