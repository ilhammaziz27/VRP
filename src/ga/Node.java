/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

/**
 *
 * @author ilhammaziz
 */
public class Node {
    private double x;
    private double y;
    private double demand;
    private String label;
    
    public Node(String label,double x,double y,double demand){
        this.label=label;
        this.x=x;
        this.y=y;
        this.demand=demand;
    }
    
    public Node(){
        this.x=0;
        this.y=0;
        this.demand=0;
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getDemand() {
        return demand;
    }

    public void setDemand(double demand) {
        this.demand = demand;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
