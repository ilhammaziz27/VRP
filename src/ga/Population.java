/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author ilhammaziz
 */
public class Population {
    private ArrayList<Chromosom> chromosoms;
    private int popSize;
    protected static Random random=new Random();
    
    public Population(ArrayList<Node> nodes, int popSize){
        this.chromosoms=new ArrayList<>();
        this.popSize=popSize;
        createPopulation(nodes);
    }
    
    public final void createPopulation(ArrayList<Node> nodes){
        Chromosom temp;
        for(int i=0;i<getPopSize();i++){
            temp=new Chromosom(nodes);
            this.chromosoms.add(temp);
        }
    }
    
    public Chromosom getParrent(){
        //Roulette Wheel
        double sumFitness=0;
        for(Chromosom chrom : chromosoms){
            sumFitness+=chrom.getFitness();
        }
        
        double rNum=randomInRange(0, sumFitness);
        
        double pSumFitness=0;
        for(Chromosom chrom : chromosoms){
            pSumFitness+=chrom.getFitness();
            if(pSumFitness>=rNum){
                return chrom;
            }
        }
        
        return null;
    }
    
    public void preCrossover(){
        int pairLength=chromosoms.size()%2==0?chromosoms.size()/2:(chromosoms.size()+1)/2;
        Chromosom cowok=new Chromosom();
        Chromosom cewek=new Chromosom();
        for(int i=1;i<=pairLength;i++){
            cowok=getParrent();
            cewek=getParrent();
            while(cewek==cowok){
                cewek=getParrent();
            }
            
            Chromosom child1=cowok;
            Chromosom child2=cewek;
            
            crossover(child1, child2);
            System.out.println("");
            //System.out.println(cewek.getFitness()+" Kawin dengan "+cowok.getFitness());
        }
    }
    
    public void crossover(Chromosom par1, Chromosom par2){
        double pc=0.8;
        double rNum=randomInRange(0, pc);
        
        Chromosom tempPar1=par2;
        Chromosom tempPar2=par1;
        
        par1.printGenes();
        par2.printGenes();
        
        if(rNum<=pc){
           //Do Crossover
           //tempPar1.printGenes();
           //tempPar2.printGenes();
           int i=0;
           
           //Crossover Parent1
            System.out.println("cycle");
            while(par1.getGenes().get(0) != par2.getGenes().get(i)){
                System.out.print(par1.getGenes().get(i).getLabel()+" ");
                tempPar1.getGenes().set(i, par1.getGenes().get(i));
                //par1.printGenes();
                //tempPar1.printGenes();
                //par1.getGenes().get(i) = tempPar1.getGenes().get(i);
                i=getGenIndex(par1, par2.getGenes().get(i));
            }
            System.out.println("");
            par1.printGenes();
        }
        System.out.println("");
    }
    
    public int getGenIndex(Chromosom chros,Node gen){
        int i=0;
        while(i<chros.getGenes().size()){
            if(chros.getGenes().get(i)==gen)
                return i;
            i++;
        }
        return -1;
    }
    
    public void showPopulation(){
        for(Chromosom individu : chromosoms){
            individu.printGenes();
            System.out.println(" -> "+individu.getFitness());
        }
    }

    public int getPopSize() {
        return popSize;
    }

    public void setPopSize(int popSize) {
        this.popSize = popSize;
    }
    
    public static double randomInRange(double min, double max) {
        double range = max - min;
        double scaled = random.nextDouble() * range;
        double shifted = scaled + min;
        return shifted; // == (rand.nextDouble() * (max-min)) + min;
    }
}
