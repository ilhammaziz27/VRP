/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

import java.util.ArrayList;

/**
 *
 * @author ilhammaziz
 */
public class VRP {
    private ArrayList<Node> nodes;
    private Population population;
    Node _0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31;
    
    private int popSize;
    private int generationSize;
    private int totalPopulation;
    
    public VRP(){
        this.nodes=new ArrayList<>();
        this.totalPopulation=0;
        this.init();
    }
    
    public void init(){
        _0=new Node("0",82,76,0);
        _1=new Node("1",96,44,19);
        _2=new Node("2",50,5,21);
        _3=new Node("3",49,8,6);
        _4=new Node("4",13,7,19);
        _5=new Node("5",29,89,7);
        _6=new Node("6",58,30,12);
        _7=new Node("7",84,39,16);
        _8=new Node("8",14,24,6);
        _9=new Node("9",2,39,16);
        _10=new Node("10",3,82,8);
        _11=new Node("11",5,10,14);
        _12=new Node("12",98,52,21);
        _13=new Node("13",84,25,16);
        _14=new Node("14",61,59,3);
        _15=new Node("15",1,65,22);
        _16=new Node("16",88,51,18);
        _17=new Node("17",91,2,19);
        _18=new Node("18",93,32,1);
        _19=new Node("29",50,3,24);
        _20=new Node("20",98,93,8);
        _21=new Node("21",5,14,12);
        _22=new Node("22",42,42,4);
        _23=new Node("23",61,9,8);
        _24=new Node("24",9,62,24);
        _25=new Node("25",80,97,24);
        _26=new Node("26",57,55,2);
        _27=new Node("27",23,69,20);
        _28=new Node("28",20,15,15);
        _29=new Node("29",20,70,2);
        _30=new Node("30",85,60,14);
        _31=new Node("31",98,5,9);
        
        nodes.add(_0);
        nodes.add(_1);
        nodes.add(_2);
        nodes.add(_3);
        nodes.add(_4);
        nodes.add(_5);
        nodes.add(_6);
        nodes.add(_7);
        nodes.add(_8);
        nodes.add(_9);
        nodes.add(_10);
        nodes.add(_11);
        nodes.add(_12);
        nodes.add(_13);
        nodes.add(_14);
        nodes.add(_15);
        nodes.add(_16);
        nodes.add(_17);
        nodes.add(_18);
        nodes.add(_19);
        nodes.add(_20);
        nodes.add(_21);
        nodes.add(_22);
        nodes.add(_23);
        nodes.add(_24);
        nodes.add(_25);
        nodes.add(_26);
        nodes.add(_27);
        nodes.add(_28);
        nodes.add(_29);
        nodes.add(_30);
        nodes.add(_31);
        
        popSize=10;
        
        ArrayList<Chromosom> chroms=new ArrayList<>();
        for(int i=0;i<popSize;i++){
            chroms.add(new Chromosom(nodes));
        }
        
        this.population=new Population(chroms, popSize);
    }
    
    public void doGA(){
        generationSize=1000;
        Chromosom best=new Chromosom();
        
        for(int i=1;i<=this.generationSize;i++){
            System.out.println("======================================");
            System.out.println("GENERATION  : "+i);
            best=population.getFittest();
            
            System.out.println("Best Route  : ");
            best.printGenes();
            System.out.println("");
            
            System.out.println("Cost        : " + best.getCost());
            System.out.println("Avg Fitness : " + population.getAvgFitness());
            System.out.println("Fittest     : " + best.getFitness());
            System.out.println("======================================");
            System.out.println("");
            
            
            if(i+1<=generationSize){
                ArrayList<Chromosom> newPop=population.crossover();
                elitism(newPop);
                survivorSelection(newPop);
                totalPopulation+=popSize;
            }
        }
        
        System.out.println("Final Solution  : ");
        best.printGenes();
        
        System.out.println("Cost            : "+best.getCost());
    }
    
    void showArrayList(ArrayList<Chromosom> chroms){
        for(Chromosom chrom : chroms){
            chrom.printGenes();
            System.out.println("Fitness : "+chrom.getFitness());
        }
    }
    
    public void elitism(ArrayList<Chromosom> newPop){
        for(Chromosom chrom : population.getChromosoms()){
            newPop.add(chrom);
        }
    }
    
    public void survivorSelection(ArrayList<Chromosom> newPop){
        //Steady State
        Chromosom temp=new Chromosom();
        for(int i=0;i<newPop.size()-1;i++){
            for(int j=i;j<newPop.size();j++){
                if(newPop.get(i).getFitness()<newPop.get(j).getFitness()){
                    temp=newPop.get(i);
                    newPop.set(i, newPop.get(j));
                    newPop.set(j, temp);
                }
            }
        }
        population=new Population(newPop, popSize);
    }
    
    public static void main(String[] args) {
        VRP driver=new VRP();
        driver.doGA();
    }
}
